#!/bin/sh

# This script is used to create a bunch of empty images of Commodore diskettes.
# It uses cc1541, which can be found here:
# https://bitbucket.org/PTV_Claus/cc1541/src/master/

if ! [ -x "$(command -v cc1541)" ]; then
    echo "cc1541 is not installed. Please install using your distro's packet manager or the source repo at https://bitbucket.org/PTV_Claus/cc1541/src/master/"
    exit 1
fi

image_version=0
image_quantity=0

echo "In case you already do have a disks/ folder in the same directory as this script, the contents will be removed! Please make sure you are aware of that!"
echo ""
echo ""

while ! [[ $image_version =~ ^[0-9]+$ ]] || [[ $image_version -gt 2 || $image_version -lt 1 ]]; do
    echo "What kind of images do you want to create?"
    echo "1: .d64 images"
    echo "2: .d81 images"
    read -n 1 image_version
    if ! [[ $image_version =~ ^[0-9]+$ ]] || [[ $image_version -gt 2 || $image_version -lt 1 ]]; then
        echo ""
        echo "Selected image type is invalid, please try again"
    fi
done

while ! [[ $image_quantity =~ ^[0-9]+$ ]] || [[ $image_quantity -gt 65536 || $image_quantity -lt 1 ]]; do
    echo ""
    echo ""
    echo "How many images do you want to create? (1-65536)"
    read -n 5 image_quantity
    if ! [[ $image_quantity =~ ^[0-9]+$ ]] || [[ $image_quantity -gt 65536 || $image_quantity -lt 1 ]]; then
        echo ""
        echo "Selected quantity is invalid, please try again"
    fi
done

filename='fseq'
seq 0 $((image_quantity - 1)) | while read n; do printf "%04x\n" $n; done > $filename

echo ""
echo "Start creation"
mkdir -p disks
rm disks/*

while read p; do
    if [[ $image_version -eq 1 ]]; then
        cc1541 -n "emptydisk" -i "${p:0:2} ${p:2:2}" disks/$p.d64
    elif [[ $image_version -eq 2 ]]; then
        cc1541 -n "emptydisk" -i "${p:0:2} ${p:2:2}" disks/$p.d81
    fi
done < $filename
rm $filename

echo "Done! You will find your files within the disks/ folder that has just been created."
