#!/bin/sh

# This script is used to bulk convert a bunch of images.
# It uses imagemagick, which can be found here:
# https://github.com/ImageMagick/ImageMagick 

if ! [ -x "$(command -v convert)" ]; then
    echo "imagemagick is not installed. Please install using your distro's packet manager or the source repo at https://github.com/ImageMagick/ImageMagick"
    exit 1
fi

origin_directory=$(pwd)
found_filetype=0;

while [[ "$source_files" == "" ]]; do
    echo "Where do you store the files that you want to be converted? (Relative paths from this directory)"
    echo "Example: /home/user/Images (leave out the slash at the end)"
    read source_files
    if [ ! -d "$source_files" ]; then
        echo "Directory does not exist, please try again"
        source_files=""
    fi
done

while [[ "$source_type" == "" ]]; do
    echo "What file type do you want to convert?"
    echo "Example: png"
    read source_type
    for i in $source_files/*.$source_type; do
        if [ -f "$i" ]; then 
            found_filetype=1
            break
        fi
    done

    if [[ ! $found_filetype == 1 ]]; then
        echo "No files of this type exist, please try again"
        source_type=""
    fi
done

while [[ "$destination_type" == "" ]]; do
    echo "Where do you store the files you wish to be converted?"
    echo "Example: webp"
    read destination_type
done

echo "Files will be converted and stored into $source_files/converted/ now. If that is okay, please enter 'y'."
read confirmation
if [[ "$confirmation" == "y" ]]; then
    echo Start conversion
    mkdir -p "$source_files/converted"
    cd "$source_files/converted"

    for file in $source_files/*.$source_type
    do
        filename=$(basename "$file")
        filename=${filename%.*}
        convert "$file" "$filename.$destination_type"
    done
    echo "Conversion has finished. You can find your files in $source_files/converted now."
else
    echo "Program aborted."
    exit 1
fi

cd $origin_directory 
