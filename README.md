# Miscellaneous scripts

Here you can find various scripts I wrote to automate tedious tasks.

## convertimages.sh

This script will bulk convert images from one filetype to another.
You will need to install [`imagemagick`](https://github.com/ImageMagick/ImageMagick) for this to work:


_**Debian/Ubuntu**_
``` bash
apt install imagemagick
```

_**Arch**_
``` bash
yay -S imagemagick
```

_**Gentoo**_
``` bash
emerge -av media-gfx/imagemagick
```

After that, run the script. It will ask for the folder where your images are located, as well as your source and destination file type, and save them under `converted/`.

**NOTE**: Make sure you enter your directory without a / at the end, such as `/home/user/Images`. Also make sure you write your extensions as `png`, `jpg`... instead of `.png`, `.jpg`...

******

## createcommodoredisks.sh

This script will create a bunch of diskette images for Commodore emulators, or devices such as the SD2IEC or Ultimate 1541. Supports `.d64` and `.d81` images.
You will need to install [`cc1541`](https://bitbucket.org/PTV_Claus/cc1541/src/master/) for this to work:

_**Debian/Ubuntu**_
``` bash
apt install cc1541
```

_**Arch**_
``` bash
yay -S cc1541
```

_**Gentoo/Source install**_
``` bash
git clone https://bitbucket.org/PTV_Claus/cc1541/src/master/
cd cc1541

make && sudo make install
```

After that, run the script. It will create your desired image type and image quantity in a seperate `disks/` folder, labeled in hexadecimal numbers.

******

## downloadvids.sh

This script will bulk download videos from a list of sites. Also supports conversion to .mp3 files.
You will need to install [`yt-dlp`](https://github.com/yt-dlp/yt-dlp) for this to work:

_**Debian/Ubuntu**_
``` bash
apt install yt-dlp
```

_**Arch**_
``` bash
yay -S yt-dlp
```

_**Gentoo**_
``` bash
emerge -av net-misc/yt-dlp
```

After that, run the script. It will ask for your link list, desired output format and target directory, and download the files into it.
