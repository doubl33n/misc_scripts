#!/bin/sh

# This script is used to download a bunch of links from a file using yt-dlp.
# It uses yt-dlp, which can be found here:
# https://github.com/yt-dlp/yt-dlp

if ! [ -x "$(command -v yt-dlp)" ]; then
    echo "yt-dlp is not installed. Please install using your distro's packet manager or the source repo at https://github.com/yt-dlp/yt-dlp"
    exit 1
fi

origin_directory=$(pwd)
download_method=0

while [[ "$target_filename" == "" ]]; do
    echo "Where is your list of links (relative paths starting from your current directory)?"
    read target_filename
    if [ ! -f "$target_filename" ]; then
        echo "File does not exist, please try again"
        target_filename=""
    fi
done

while ! [[ $download_method =~ ^[0-9]+$ ]] || [[ $download_method -gt 3 || $download_method -lt 1 ]]; do
    echo "Which download method do you prefer?"
    echo "1: simple download (best video & audio quality)"
    echo "2: download as mp4"
    echo "3: download only audio as mp3"
    read -n 1 download_method
    echo ""
    if ! [[ $download_method =~ ^[0-9]+$ ]] || [[ $download_method -gt 3 || $download_method -lt 1 ]]; then
        echo "Selected download method is invalid, please try again"
    fi
done

while [[ "$target_directory" == "" ]]; do
    echo "Please enter the directory the files should be downloaded to:"
    read target_directory
    mkdir -p $target_directory
    if [ ! -d "$target_directory" ]; then
        echo "Directory does not exist, please try again"
        target_directory=""
    fi
done

echo "Files will be downloaded to $target_directory now. If that is okay, please enter 'y'."
read confirmation
if [[ "$confirmation" == "y" ]]; then
    echo Start download
    cd "$target_directory"
    while read p; do
        if [[ $download_method -eq 1 ]]; then
            yt-dlp -wf 'bv*+ba/b' -o '%(title)s.%(ext)s' $p # Simple download
        elif [[ $download_method -eq 2 ]]; then
            yt-dlp -wS res,ext:mp4:m4a --recode mp4 -o '%(title)s.%(ext)s' $p # MP4 download
        elif [[ $download_method -eq 3 ]]; then
            yt-dlp -wx --audio-format mp3 -f 'ba' -o '%(title)s.%(ext)s' $p #MP3 download
        fi
    done < $target_filename
    echo "Download has finished. You can find your files in $target_directory now."
else
    echo "Program aborted."
    exit 1
fi

cd $origin_directory 
